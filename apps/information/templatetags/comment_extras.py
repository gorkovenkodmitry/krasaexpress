#coding: utf-8
from django import template

register = template.Library()

@register.filter(name='template_mult')
def template_mult(value, arg):
    try:
        a = float(value)
        b = float(arg)
        return u'%.2f' % (a * b)
    except:
        return value