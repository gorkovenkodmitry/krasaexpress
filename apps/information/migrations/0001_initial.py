# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SiteOrder'
        db.create_table(u'information_siteorder', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pub_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('date_order', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('telephone', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('status', self.gf('django.db.models.fields.CharField')(default=u'new', max_length=100)),
            ('url', self.gf('django.db.models.fields.CharField')(default=u'', max_length=250, blank=True)),
        ))
        db.send_create_signal(u'information', ['SiteOrder'])


    def backwards(self, orm):
        # Deleting model 'SiteOrder'
        db.delete_table(u'information_siteorder')


    models = {
        u'information.siteorder': {
            'Meta': {'ordering': "['-pub_date']", 'object_name': 'SiteOrder'},
            'date_order': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'new'", 'max_length': '100'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'url': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '250', 'blank': 'True'})
        }
    }

    complete_apps = ['information']