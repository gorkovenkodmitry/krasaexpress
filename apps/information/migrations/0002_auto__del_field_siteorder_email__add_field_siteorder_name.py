# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'SiteOrder.email'
        db.delete_column(u'information_siteorder', 'email')

        # Adding field 'SiteOrder.name'
        db.add_column(u'information_siteorder', 'name',
                      self.gf('django.db.models.fields.CharField')(default=u'', max_length=250),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'SiteOrder.email'
        db.add_column(u'information_siteorder', 'email',
                      self.gf('django.db.models.fields.EmailField')(default=1, max_length=75),
                      keep_default=False)

        # Deleting field 'SiteOrder.name'
        db.delete_column(u'information_siteorder', 'name')


    models = {
        u'information.siteorder': {
            'Meta': {'ordering': "['-pub_date']", 'object_name': 'SiteOrder'},
            'date_order': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '250'}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'new'", 'max_length': '100'}),
            'telephone': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'url': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '250', 'blank': 'True'})
        }
    }

    complete_apps = ['information']