#coding: utf-8
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.http import HttpResponseBadRequest, HttpResponse
from django.template import loader, Context
from django.views.generic.base import TemplateView, View
from forms import SiteOrderForm, PriceOrderForm, ServiceOrderForm, ActionOrderForm, ReviewOrderForm, QuestionOrderForm
from apps.siteblocks.models import Settings
import settings
from apps.classes import send_sms
from config import production


def send_email(type_order, new_order):
    try:
        site_email = Settings.objects.get(name='site_email')
    except Settings.DoesNotExist:
        site_email = False

    if site_email:
        if site_email.value:
            current_site = Site.objects.get_current()
            current_site = current_site.name

            subject = u'%s - Новая заявка на сайте' % current_site
            subject = u''.join(subject.splitlines())
            subject = subject

            mt = loader.get_template('order_message.html')
            mc = Context({
                'new_order': new_order,
                'type_order': type_order,
            })
            message = mt.render(mc).encode('utf-8')
            msg = EmailMessage(subject, message, production.EMAIL_HOST_USER, [site_email.value])
            msg.content_subtype = "html"
            msg.send()
    return True


class SiteOrderSend(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        data = request.POST.copy()
        form = SiteOrderForm(data)
        if form.is_valid():
            new_order = form.save()
            try:
                site_order_phone = Settings.objects.get(name='site_order_phone').value
            except Settings.DoesNotExist:
                site_order_phone = False
            try:
                sms_api = Settings.objects.get(name='sms_api').value
            except Settings.DoesNotExist:
                sms_api = False
            if site_order_phone and sms_api:
                text = u'Новая заяка на сайте: %s, имя: %s' % (new_order.telephone, new_order.name)
                print sms_api
                send_sms(site_order_phone, sms_api, text)

            type_order = u'Заявка на выезда мастера на дату: <b>%s</b>' % new_order.date_order.strftime('%d.%m.%Y')
            send_email(type_order, new_order)
            return HttpResponse('success')
        else:
            return HttpResponse('error')


class PriceOrderSend(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        data = request.POST.copy()
        form = PriceOrderForm(data)
        if form.is_valid():
            new_order = form.save()
            try:
                site_order_phone = Settings.objects.get(name='site_order_phone').value
            except Settings.DoesNotExist:
                site_order_phone = False
            try:
                sms_api = Settings.objects.get(name='sms_api').value
            except Settings.DoesNotExist:
                sms_api = False
            if site_order_phone and sms_api:
                text = u'Новая заяка на сайте: %s, имя: %s' % (new_order.telephone, new_order.name)
                print sms_api
                send_sms(site_order_phone, sms_api, text)

            type_order = u'Заявка на прайс лист, email: <b>%s</b>' % new_order.email
            send_email(type_order, new_order)
            return HttpResponse('success')
        else:
            return HttpResponse('error')


class ServiceOrderSend(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        data = request.POST.copy()
        form = ServiceOrderForm(data)
        if form.is_valid():
            new_order = form.save()
            try:
                site_order_phone = Settings.objects.get(name='site_order_phone').value
            except Settings.DoesNotExist:
                site_order_phone = False
            try:
                sms_api = Settings.objects.get(name='sms_api').value
            except Settings.DoesNotExist:
                sms_api = False
            if site_order_phone and sms_api:
                text = u'Новая заяка на сайте: %s, имя: %s' % (new_order.telephone, new_order.name)
                print sms_api
                send_sms(site_order_phone, sms_api, text)

            type_order = u'Заявка на заказ услуги, email: <b>%s</b>' % new_order.email
            send_email(type_order, new_order)
            return HttpResponse('success')
        else:
            return HttpResponse('error')


class ActionOrderSend(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        data = request.POST.copy()
        form = ActionOrderForm(data)
        if form.is_valid():
            new_order = form.save()
            try:
                site_order_phone = Settings.objects.get(name='site_order_phone').value
            except Settings.DoesNotExist:
                site_order_phone = False
            try:
                sms_api = Settings.objects.get(name='sms_api').value
            except Settings.DoesNotExist:
                sms_api = False
            if site_order_phone and sms_api:
                text = u'Новая заяка на сайте: %s, имя: %s' % (new_order.telephone, new_order.name)
                print sms_api
                send_sms(site_order_phone, sms_api, text)

            type_order = u'Заявка на информацию об акциях, email: <b>%s</b>' % new_order.email
            send_email(type_order, new_order)
            return HttpResponse('success')
        else:
            return HttpResponse('error')


class ReviewOrderSend(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        data = request.POST.copy()
        form = ReviewOrderForm(data)
        if form.is_valid():
            new_order = form.save()
            try:
                site_order_phone = Settings.objects.get(name='site_order_phone').value
            except Settings.DoesNotExist:
                site_order_phone = False
            try:
                sms_api = Settings.objects.get(name='sms_api').value
            except Settings.DoesNotExist:
                sms_api = False
            if site_order_phone and sms_api:
                text = u'Новая заяка на сайте: %s, имя: %s' % (new_order.telephone, new_order.name)
                print sms_api
                send_sms(site_order_phone, sms_api, text)

            type_order = u'Заявка о желании оставить отзыв, email: <b>%s</b>' % new_order.email
            send_email(type_order, new_order)
            return HttpResponse('success')
        else:
            return HttpResponse('error')


class QuestionOrderSend(View):
    def post(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        data = request.POST.copy()
        form = QuestionOrderForm(data)
        if form.is_valid():
            new_order = form.save()
            try:
                site_order_phone = Settings.objects.get(name='site_order_phone').value
            except Settings.DoesNotExist:
                site_order_phone = False
            try:
                sms_api = Settings.objects.get(name='sms_api').value
            except Settings.DoesNotExist:
                sms_api = False
            if site_order_phone and sms_api:
                text = u'Новая заяка на сайте: %s, имя: %s' % (new_order.telephone, new_order.name)
                print sms_api
                send_sms(site_order_phone, sms_api, text)

            type_order = u'Заявка на другой вопрос, email: <b>%s</b>' % new_order.email
            send_email(type_order, new_order)
            return HttpResponse('success')
        else:
            return HttpResponse('error')