#coding: utf-8
import os
import datetime
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db import models

status_choises = (
    (u'new', u'Новая'),
    (u'ready', u'Обработана'),
)


class SiteOrder(models.Model):
    pub_date = models.DateTimeField(verbose_name=u'Дата', default=datetime.datetime.now)
    date_order = models.DateTimeField(verbose_name=u'Дата выезда', default=datetime.datetime.now)
    telephone = models.CharField(verbose_name=u'Телефон', max_length=20)
    name = models.CharField(verbose_name=u'Имя', max_length=250, default=u'')
    status = models.CharField(max_length=100, verbose_name=u'Статус', choices=status_choises, default=u'new')
    url = models.CharField(verbose_name=u'Откуда пришел', blank=True, default=u'', max_length=250)

    objects = models.Manager()

    class Meta:
        verbose_name = _(u'order_item')
        verbose_name_plural = _(u'order_items')
        ordering = ['-pub_date']

    def __unicode__(self):
        return u'Дата заявки от %s' % self.pub_date


class PriceOrder(models.Model):
    pub_date = models.DateTimeField(verbose_name=u'Дата', default=datetime.datetime.now)
    telephone = models.CharField(verbose_name=u'Телефон', max_length=20)
    name = models.CharField(verbose_name=u'Имя', max_length=250, default=u'')
    email = models.EmailField(verbose_name=u'E-mail')
    status = models.CharField(max_length=100, verbose_name=u'Статус', choices=status_choises, default=u'new')
    url = models.CharField(verbose_name=u'Откуда пришел', blank=True, default=u'', max_length=250)

    objects = models.Manager()

    class Meta:
        verbose_name = _(u'price_order')
        verbose_name_plural = _(u'price_orders')
        ordering = ['-pub_date']

    def __unicode__(self):
        return u'Дата заявки от %s' % self.pub_date


class ServiceOrder(models.Model):
    pub_date = models.DateTimeField(verbose_name=u'Дата', default=datetime.datetime.now)
    telephone = models.CharField(verbose_name=u'Телефон', max_length=20)
    name = models.CharField(verbose_name=u'Имя', max_length=250, default=u'')
    email = models.EmailField(verbose_name=u'E-mail')
    status = models.CharField(max_length=100, verbose_name=u'Статус', choices=status_choises, default=u'new')
    url = models.CharField(verbose_name=u'Откуда пришел', blank=True, default=u'', max_length=250)

    objects = models.Manager()

    class Meta:
        verbose_name = _(u'service_order')
        verbose_name_plural = _(u'service_orders')
        ordering = ['-pub_date']

    def __unicode__(self):
        return u'Дата заявки от %s' % self.pub_date


class ActionOrder(models.Model):
    pub_date = models.DateTimeField(verbose_name=u'Дата', default=datetime.datetime.now)
    telephone = models.CharField(verbose_name=u'Телефон', max_length=20)
    name = models.CharField(verbose_name=u'Имя', max_length=250, default=u'')
    email = models.EmailField(verbose_name=u'E-mail')
    status = models.CharField(max_length=100, verbose_name=u'Статус', choices=status_choises, default=u'new')
    url = models.CharField(verbose_name=u'Откуда пришел', blank=True, default=u'', max_length=250)

    objects = models.Manager()

    class Meta:
        verbose_name = _(u'action_order')
        verbose_name_plural = _(u'action_orders')
        ordering = ['-pub_date']

    def __unicode__(self):
        return u'Дата заявки от %s' % self.pub_date


class ReviewOrder(models.Model):
    pub_date = models.DateTimeField(verbose_name=u'Дата', default=datetime.datetime.now)
    telephone = models.CharField(verbose_name=u'Телефон', max_length=20)
    name = models.CharField(verbose_name=u'Имя', max_length=250, default=u'')
    email = models.EmailField(verbose_name=u'E-mail')
    status = models.CharField(max_length=100, verbose_name=u'Статус', choices=status_choises, default=u'new')
    url = models.CharField(verbose_name=u'Откуда пришел', blank=True, default=u'', max_length=250)

    objects = models.Manager()

    class Meta:
        verbose_name = _(u'review_order')
        verbose_name_plural = _(u'review_orders')
        ordering = ['-pub_date']

    def __unicode__(self):
        return u'Дата заявки от %s' % self.pub_date


class QuestionOrder(models.Model):
    pub_date = models.DateTimeField(verbose_name=u'Дата', default=datetime.datetime.now)
    telephone = models.CharField(verbose_name=u'Телефон', max_length=20)
    name = models.CharField(verbose_name=u'Имя', max_length=250, default=u'')
    email = models.EmailField(verbose_name=u'E-mail')
    status = models.CharField(max_length=100, verbose_name=u'Статус', choices=status_choises, default=u'new')
    url = models.CharField(verbose_name=u'Откуда пришел', blank=True, default=u'', max_length=250)

    objects = models.Manager()

    class Meta:
        verbose_name = _(u'question_order')
        verbose_name_plural = _(u'question_orders')
        ordering = ['-pub_date']

    def __unicode__(self):
        return u'Дата заявки от %s' % self.pub_date