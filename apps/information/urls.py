# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns
from django.views.decorators.csrf import csrf_exempt
from views import SiteOrderSend, PriceOrderSend, ServiceOrderSend, ActionOrderSend, ReviewOrderSend, QuestionOrderSend


urlpatterns = patterns('',
	url(r'^site_order/send/$', csrf_exempt(SiteOrderSend.as_view()) , name='site_order_send'),
	url(r'^price_order/send/$', csrf_exempt(PriceOrderSend.as_view()) , name='price_order_send'),
	url(r'^service_order/send/$', csrf_exempt(ServiceOrderSend.as_view()) , name='service_order_send'),
	url(r'^action_order/send/$', csrf_exempt(ActionOrderSend.as_view()) , name='action_order_send'),
	url(r'^review_order/send/$', csrf_exempt(ReviewOrderSend.as_view()) , name='review_order_send'),
	url(r'^q_order/send/$', csrf_exempt(QuestionOrderSend.as_view()) , name='q_order_send'),
)