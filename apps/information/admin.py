#coding: utf-8
from django.contrib import admin
from models import SiteOrder, PriceOrder, ServiceOrder, ActionOrder, ReviewOrder, QuestionOrder


class SiteOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'pub_date', 'name', 'telephone', 'status', )
    list_display_links = ('id', 'pub_date', 'name', 'telephone', )
    list_filter = ('pub_date', 'status', )
    date_hierarchy = 'pub_date'
    search_fields = ('name', 'telephone', )
    readonly_fields = ('url', )

admin.site.register(SiteOrder, SiteOrderAdmin)
admin.site.register(PriceOrder, SiteOrderAdmin)
admin.site.register(ServiceOrder, SiteOrderAdmin)
admin.site.register(ActionOrder, SiteOrderAdmin)
admin.site.register(ReviewOrder, SiteOrderAdmin)
admin.site.register(QuestionOrder, SiteOrderAdmin)