#coding: utf-8
from django import forms
from models import SiteOrder, PriceOrder, ServiceOrder, ActionOrder, ReviewOrder, QuestionOrder


class SiteOrderForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput({'placeholder': u'Ваше имя', 'id': ''}), required=True)
    telephone = forms.CharField(widget=forms.TextInput({'placeholder': u'Номер телефона', 'id': ''}), required=True)
    url = forms.CharField(widget=forms.HiddenInput({'id': ''}), required=False)
    date_order = forms.DateField(widget=forms.TextInput({'placeholder': u'Дата выезда', 'id': ''}), required=True)

    class Meta:
        model = SiteOrder
        fields = ('name', 'telephone', 'url', 'date_order', )


class PriceOrderForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput({'placeholder': u'Ваше имя', 'id': ''}), required=True)
    telephone = forms.CharField(widget=forms.TextInput({'placeholder': u'Номер телефона', 'id': ''}), required=True)
    url = forms.CharField(widget=forms.HiddenInput({'id': ''}), required=False)
    email = forms.CharField(widget=forms.TextInput({'placeholder': u'Электронная почта', 'id': ''}), required=True)

    class Meta:
        model = PriceOrder
        fields = ('name', 'telephone', 'url', 'email', )


class ServiceOrderForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput({'placeholder': u'Ваше имя', 'id': ''}), required=True)
    telephone = forms.CharField(widget=forms.TextInput({'placeholder': u'Номер телефона', 'id': ''}), required=True)
    url = forms.CharField(widget=forms.HiddenInput({'id': ''}), required=False)
    email = forms.CharField(widget=forms.TextInput({'placeholder': u'Электронная почта', 'id': ''}), required=True)

    class Meta:
        model = ServiceOrder
        fields = ('name', 'telephone', 'url', 'email', )


class ActionOrderForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput({'placeholder': u'Ваше имя', 'id': ''}), required=True)
    telephone = forms.CharField(widget=forms.TextInput({'placeholder': u'Номер телефона', 'id': ''}), required=True)
    url = forms.CharField(widget=forms.HiddenInput({'id': ''}), required=False)
    email = forms.CharField(widget=forms.TextInput({'placeholder': u'Электронная почта', 'id': ''}), required=True)

    class Meta:
        model = ActionOrder
        fields = ('name', 'telephone', 'url', 'email', )


class ReviewOrderForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput({'placeholder': u'Ваше имя', 'id': ''}), required=True)
    telephone = forms.CharField(widget=forms.TextInput({'placeholder': u'Номер телефона', 'id': ''}), required=True)
    url = forms.CharField(widget=forms.HiddenInput({'id': ''}), required=False)
    email = forms.CharField(widget=forms.TextInput({'placeholder': u'Электронная почта', 'id': ''}), required=True)

    class Meta:
        model = ReviewOrder
        fields = ('name', 'telephone', 'url', 'email', )


class QuestionOrderForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput({'placeholder': u'Ваше имя', 'id': ''}), required=True)
    telephone = forms.CharField(widget=forms.TextInput({'placeholder': u'Номер телефона', 'id': ''}), required=True)
    url = forms.CharField(widget=forms.HiddenInput({'id': ''}), required=False)
    email = forms.CharField(widget=forms.TextInput({'placeholder': u'Электронная почта', 'id': ''}), required=True)

    class Meta:
        model = QuestionOrder
        fields = ('name', 'telephone', 'url', 'email', )