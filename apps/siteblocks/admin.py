# -*- coding: utf-8 -*-
import os
from django.contrib import admin
from django import forms
from django.core.files.base import ContentFile
from django.forms.models import save_instance
from salmonella.admin import SalmonellaMixin
from sorl.thumbnail import get_thumbnail
from django.core.files.storage import default_storage
from apps.siteblocks.models import Settings, Service, Project, Action, Review, SocialLink
from config.base import MEDIA_ROOT
from django.utils.safestring import mark_safe
from pytils.translit import translify
from sorl.thumbnail.admin import AdminImageMixin, AdminInlineImageMixin

from apps.utils.widgets import Redactor


def file_path_Price(instance, filename):
    return os.path.join('images', 'settings',  translify(filename).replace(' ', '_') )


class AdminImageWidget(forms.FileInput):
    def __init__(self, attrs={}):
        super(AdminImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        output = []
        if value:
            im = get_thumbnail(value, 'x150', upscale=0, quality=99)
            output.append(('<a target="_blank" href="%s">'
                           '<img src="%s" style="height: 150px;" /></a> '
                           % (im.url, im.url)))
        output.append(super(AdminImageWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class SettingsAdminForm(forms.ModelForm):
    class Meta:
        model = Settings

    def __init__(self, *args, **kwargs):
        super(SettingsAdminForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
        except KeyError:
            instance = False
        if instance:
            if instance.type == u'input':
                self.fields['value'].widget = forms.TextInput()
            elif instance.type == u'textarea':
                self.fields['value'].widget = forms.Textarea()
            elif instance.type == u'redactor':
                self.fields['value'].widget = Redactor(attrs={'cols': 100, 'rows': 10},)
            elif instance.type == u'file':
                self.fields['value'].widget = AdminImageWidget()

    def save(self, commit=True):
        if self.instance.pk is None:
            fail_message = 'created'
        else:
            fail_message = 'changed'
        if self.cleaned_data['type'] == u'file' and self.files:
            filename = self.instance.value
            file_path = MEDIA_ROOT + os.path.join('/images', 'settings',  translify(filename).replace(' ', '_'))
            file_path_field = os.path.join('images', 'settings',  translify(filename).replace(' ', '_'))
            file = self.files['value']
            default_storage.save(file_path, ContentFile(file.read()))
            self.instance.file = file_path_field
        if self.cleaned_data['type'] == u'file':
            self.instance.value=self.instance.file
        return save_instance(self, self.instance, self._meta.fields,
                             fail_message, commit, construct=False)


class SettingsAdmin(admin.ModelAdmin):
    list_display = ('title', 'name', 'value',)
    form = SettingsAdminForm
    #readonly_fields = ('name', 'type',)
admin.site.register(Settings, SettingsAdmin)


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'admin_image_preview', 'order', 'is_published', )
    list_display_links = ('id', 'name', 'admin_image_preview',)
    list_editable = ('order', 'is_published', )

admin.site.register(Service, ServiceAdmin)
admin.site.register(Project, ServiceAdmin)
admin.site.register(Action, ServiceAdmin)
admin.site.register(SocialLink, ServiceAdmin)


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'admin_image_preview', 'image', 'order', 'is_published', )
    list_display_links = ('id', 'admin_image_preview', 'image', )
    list_editable = ('order', 'is_published', )

admin.site.register(Review, ReviewAdmin)