# -*- coding: utf-8 -*-choreographers
import os
from django.utils.translation import ugettext_lazy as _
from pytils.translit import translify
from sorl.thumbnail import ImageField as sorl_ImageField, get_thumbnail
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from apps.utils.custom_mptt_classes import TreeManager, TreeManagerVisible
from django.db import models
from apps.utils.managers import VisibleObjects


type_choices = (
    (u'input', u'input'),
    (u'textarea', u'textarea'),
    (u'redactor', u'redactor'),
    (u'file', u'file'),
)


class ImageField(sorl_ImageField, models.ImageField):
    pass


def file_path_Service(instance, filename):
    return os.path.join('images', 'services',  translify(filename).replace(' ', '_'))


def file_path_Project(instance, filename):
    return os.path.join('images', 'projects',  translify(filename).replace(' ', '_'))


def file_path_Action(instance, filename):
    return os.path.join('images', 'actions',  translify(filename).replace(' ', '_'))


def file_path_Review(instance, filename):
    return os.path.join('images', 'reviews',  translify(filename).replace(' ', '_'))


def file_path_SocialLink(instance, filename):
    return os.path.join('images', 'social_links',  translify(filename).replace(' ', '_'))


class Settings(models.Model):
    title = models.CharField(verbose_name=u'Название', max_length=150,)
    name = models.CharField(verbose_name=u'Служебное имя', max_length=250,)
    value = models.TextField(verbose_name=u'Значение', blank=True)
    file = models.CharField(max_length=300, editable=False, blank=True)
    type = models.CharField(max_length=20, verbose_name=u'Тип значения', choices=type_choices)

    class Meta:
        verbose_name = _(u'site_setting')
        verbose_name_plural = _(u'site_settings')

    def __unicode__(self):
        return u'%s' % self.name


class Service(models.Model):
    name = models.CharField(verbose_name=u'Услуга', max_length=250,)
    image = ImageField(verbose_name=u'Изображение', upload_to=file_path_Service)
    order = models.IntegerField(verbose_name=u'Порядок сортировки', default=10)
    is_published = models.BooleanField(default=True, verbose_name=u'Опубликовать')

    objects = models.Manager()
    items = VisibleObjects()

    class Meta:
        ordering = ('-order',)
        verbose_name = _(u'service')
        verbose_name_plural = _(u'services')

    def __unicode__(self):
        return u'%s' % self.name

    def admin_image_preview(self):
        image = self.image
        if image:
            im = get_thumbnail(self.image, '96x96', crop='center', quality=80, format='PNG')
            return u'<span><img src="%s" width="96" height="96" /></span>' % im.url
        else:
            return u'<span></span>'
    admin_image_preview.allow_tags = True
    admin_image_preview.short_description = u'Превью'


class Project(models.Model):
    name = models.CharField(verbose_name=u'Наши работы', max_length=250, blank=True)
    image = ImageField(verbose_name=u'Изображение', upload_to=file_path_Project)
    order = models.IntegerField(verbose_name=u'Порядок сортировки', default=10)
    is_published = models.BooleanField(default=True, verbose_name=u'Опубликовать')

    objects = models.Manager()
    items = VisibleObjects()

    class Meta:
        ordering = ('-order',)
        verbose_name = _(u'project')
        verbose_name_plural = _(u'projects')

    def __unicode__(self):
        return u'%s' % self.name

    def admin_image_preview(self):
        image = self.image
        if image:
            im = get_thumbnail(self.image, '96x96', crop='center', quality=80, format='PNG')
            return u'<span><img src="%s" width="96" height="96" /></span>' % im.url
        else:
            return u'<span></span>'
    admin_image_preview.allow_tags = True
    admin_image_preview.short_description = u'Превью'


class Action(models.Model):
    name = models.TextField(verbose_name=u'Акция',)
    image = ImageField(verbose_name=u'Изображение', upload_to=file_path_Action)
    order = models.IntegerField(verbose_name=u'Порядок сортировки', default=10)
    is_published = models.BooleanField(default=True, verbose_name=u'Опубликовать')

    objects = models.Manager()
    items = VisibleObjects()

    class Meta:
        ordering = ('-order',)
        verbose_name = _(u'action_item')
        verbose_name_plural = _(u'action_items')

    def __unicode__(self):
        return u'%s' % self.name

    def admin_image_preview(self):
        image = self.image
        if image:
            im = get_thumbnail(self.image, '96x96', crop='center', quality=80, format='PNG')
            return u'<span><img src="%s" width="96" height="96" /></span>' % im.url
        else:
            return u'<span></span>'
    admin_image_preview.allow_tags = True
    admin_image_preview.short_description = u'Превью'


class Review(models.Model):
    text = models.TextField(verbose_name=u'Отзыв',)
    image = ImageField(verbose_name=u'Изображение', upload_to=file_path_Action)
    order = models.IntegerField(verbose_name=u'Порядок сортировки', default=10)
    is_published = models.BooleanField(default=True, verbose_name=u'Опубликовать')

    objects = models.Manager()
    items = VisibleObjects()

    class Meta:
        ordering = ('-order',)
        verbose_name = _(u'review')
        verbose_name_plural = _(u'reviews')

    def __unicode__(self):
        return u'%s' % self.pk

    def admin_image_preview(self):
        image = self.image
        if image:
            im = get_thumbnail(self.image, '96x96', crop='center', quality=80, format='PNG')
            return u'<span><img src="%s" width="96" height="96" /></span>' % im.url
        else:
            return u'<span></span>'
    admin_image_preview.allow_tags = True
    admin_image_preview.short_description = u'Превью'


class SocialLink(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=250,)
    url = models.URLField(verbose_name=u'Ссылка')
    image = ImageField(verbose_name=u'Изображение', upload_to=file_path_SocialLink)
    order = models.IntegerField(verbose_name=u'Порядок сортировки', default=10)
    is_published = models.BooleanField(default=True, verbose_name=u'Опубликовать')

    objects = models.Manager()
    items = VisibleObjects()

    class Meta:
        ordering = ('-order',)
        verbose_name = _(u'social_link')
        verbose_name_plural = _(u'social_links')

    def admin_image_preview(self):
        image = self.image
        if image:
            im = get_thumbnail(self.image, '30x30', crop='center', quality=80, format='PNG')
            return u'<span><img src="%s" width="30" height="30" /></span>' % im.url
        else:
            return u'<span></span>'
    admin_image_preview.allow_tags = True
    admin_image_preview.short_description = u'Превью'