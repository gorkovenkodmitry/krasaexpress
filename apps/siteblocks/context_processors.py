# -*- coding: utf-8 -*-
from apps.siteblocks.models import Settings, SocialLink
from settings import SITE_NAME


def settings(request):
    current_url = request.path
    all_settings = Settings.objects.all()
    links = SocialLink.items.all()[::-1]

    try:
        site_title = all_settings.get(name='site_title').value
    except Settings.DoesNotExist:
        site_title = ''
    try:
        yandex_metrika = all_settings.get(name='yandex_metrika').value
    except Settings.DoesNotExist:
        yandex_metrika = ''

    return {
        'site_name': SITE_NAME,
        'site_title': site_title,
        'links': links,
        'yandex_metrika': yandex_metrika,
    }