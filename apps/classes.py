#coding: utf-8

import re
import urllib2
from django.db import models
from django.http import HttpResponse
from django.template.loader import render_to_string
from sorl.thumbnail import ImageField as sorl_ImageField

import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.contrib.sites.models import Site


class ImageField(sorl_ImageField, models.ImageField):
    pass


def send_email(email_title, email_template, dictionary, receivers):
    try:
        current_site = Site.objects.get_current()
        subject = u'%s - %s' % (current_site.name, email_title)
        subject = u''.join(subject.splitlines())

        message = render_to_string(email_template, dictionary).encode('utf-8')

        msg = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, receivers)
        msg.content_subtype = "html"
        msg.send()
        return True
    except:
        return False


def send_sms(sender, api, text, send_from=False):
    text = urllib2.quote(text.encode('utf-8'))
    url = u'http://sms.ru/sms/send?api_id=%s&to=%s&text=%s' % (api, sender, text)
    if send_from:
        url += u'&from=%s' % urllib2.quote(send_from.encode('utf-8'))
    result = urllib2.urlopen(url).read()
    print result
    return


def get_discount_html(text):
    pattern = r'\d+%'
    t = text.splitlines()
    result = ""
    for i in t:
        z = re.findall(pattern, i)
        ss = i.replace(z[0], '')
        result += """<div class="discount_item"><span>%s</span>%s</div>\n""" % (z[0], ss)
    return result


def get_text_html(text):
    pattern = r'^<p.*?>(.*)</p>'
    result = re.findall(pattern, text)

    pattern = r'<[^>]+>'
    deleted = re.findall(pattern, result[0])
    result = result[0]
    for i in deleted:
        result = result.replace(i, "")
    return result
