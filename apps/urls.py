# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from views import IndexView, RobotView
from apps.information.urls import urlpatterns as information_urls

urlpatterns = patterns('',
                       url(r'^$', IndexView.as_view(), name='index'),
                       (r'^robots.txt$', RobotView.as_view()),
                       )

urlpatterns += information_urls