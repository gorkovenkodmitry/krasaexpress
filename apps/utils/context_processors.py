# -*- coding: utf-8 -*-

def custom_proc(request):
    current_url = request.path

    main_url = '/'
    return {
        'current_url': current_url,
        'main_url': main_url,
    }