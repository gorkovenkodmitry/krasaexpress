# -*- coding: utf-8 -*-
from mptt.models import MPTTModel as mptt_MPTTModel, TreeManager as mpttTreeManager
from mptt.admin import MPTTModelAdmin as mptt_MPTTModelAdmin
from django.contrib.admin.views.main import ChangeList
import django
from apps.utils.managers import PublishedMixin


class MPTTChangeList(ChangeList):
    def get_query_set(self, request=None):
        # request arg was added in django r16144 (after 1.3)
        if request is not None and django.VERSION >= (1, 4):
            qs = super(MPTTChangeList, self).get_query_set(request)
        else:
            qs = super(MPTTChangeList, self).get_query_set()

        # always order by (tree_id, left)
        tree_id = qs.model._mptt_meta.tree_id_attr
        left = qs.model._mptt_meta.left_attr
        return qs.order_by('-'+tree_id, left)


class MPTTModelAdmin(mptt_MPTTModelAdmin):
    def get_changelist(self, request, **kwargs):
        return MPTTChangeList


class TreeManager(PublishedMixin, mpttTreeManager):
    def get_query_set(self):
        return super(TreeManager, self).get_query_set().order_by('-' + self.tree_id_attr, self.left_attr)


class TreeManagerVisible(PublishedMixin, mpttTreeManager):
    def get_query_set(self):
        return super(TreeManagerVisible, self).get_query_set().order_by('-'+self.tree_id_attr,
                                                                        self.left_attr).filter(is_published=True)