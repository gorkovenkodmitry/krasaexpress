#coding: utf-8
import datetime
from django.views.generic import TemplateView
from apps.siteblocks.models import Service, Project, Action, Review
from apps.information.forms import SiteOrderForm, PriceOrderForm, ServiceOrderForm, ActionOrderForm, ReviewOrderForm, QuestionOrderForm


def get_list(a):
    b = []
    row = []
    for x in a:
        row.append(x)
        if len(row) == 3:
            b.append(row)
            row = []
    else:
        if len(row) > 0:
            b.append(row)
    return b

class IndexView(TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        form = SiteOrderForm()
        http_referer = request.META['HTTP_REFERER'] if 'HTTP_REFERER' in request.META else ''
        form.fields['url'].initial = http_referer
        context['form'] = form

        price_form = PriceOrderForm()
        price_form.fields['url'].initial = http_referer
        context['price_form'] = price_form

        service_form = ServiceOrderForm()
        service_form.fields['url'].initial = http_referer
        context['service_form'] = service_form

        action_form = ActionOrderForm()
        action_form.fields['url'].initial = http_referer
        context['action_form'] = action_form

        review_form = ReviewOrderForm()
        review_form.fields['url'].initial = http_referer
        context['review_form'] = review_form

        q_form = QuestionOrderForm()
        q_form.fields['url'].initial = http_referer
        context['q_form'] = q_form
        
        context['services'] = get_list(Service.items.all())
        context['projects'] = get_list(Project.items.all())
        context['actions'] = get_list(Action.items.all())
        context['reviews'] = Review.items.all()
        return self.render_to_response(context)


class RobotView(TemplateView):
    template_name = 'robot.txt'