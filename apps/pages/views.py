# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader, RequestContext
from apps.pages.models import Page


def page(request, url):
    if not url.endswith('/'):
        return HttpResponseRedirect("%s/" % request.path)
    if not url.startswith('/'):
        url = "/" + url
    page = get_object_or_404(Page, url__exact=url, is_published=True)
    t = loader.get_template(u'pages/%s' % page.template)
    c = RequestContext(request, locals())
    return HttpResponse(t.render(c))