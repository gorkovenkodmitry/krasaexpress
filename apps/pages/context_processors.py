# -*- coding: utf-8 -*-
from apps.pages.models import MetaData


def meta(request):
    try:
        current_url = request.path
        meta = MetaData.objects.get(url=current_url)
    except MetaData.DoesNotExist:
        meta = False

    return {
        'meta': meta,
    }