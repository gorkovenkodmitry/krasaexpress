# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from apps.pages.models import Page, MetaData, PageDoc, PagePic
from apps.utils.widgets import Redactor, AdminImageWidget
from sorl.thumbnail.admin import AdminImageMixin
from mptt.admin import MPTTModelAdmin
import reversion


class PageAdminForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PageAdminForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs['instance']
        except KeyError:
            instance = False
            self.fields['content'].widget = Redactor(attrs={'cols': 170, 'rows': 20},)
        if instance:
            if instance.type == u'textarea':
                self.fields['content'].widget = forms.Textarea(attrs={'cols': 100, 'rows': 20},)
            elif instance.type == u'redactor':
                self.fields['content'].widget = Redactor(attrs={'cols': 170, 'rows': 20},)

    class Meta:
        model = Page


class PageAdmin(AdminImageMixin, MPTTModelAdmin):
    list_display = ('title', 'url', 'order', 'is_published',)
    list_display_links = ('title', 'url',)
    list_editable = ('is_published', 'order',)
    search_fields = ('title', 'url', 'content',)
    list_select_related = True
    exclude = ('parent',)
    fieldsets = (
        (u'Основные поля', {
            'fields': ('title', 'url', 'content', 'type', 'template',),
        }),
        (u'Параметры публикации и сортировки', {
            'fields': ('order', 'is_published',),
        }),
        (u'SEO элементы', {
            'fields': ('page_title', 'description', 'keywords')
        })
    )
    form = PageAdminForm

    #inlines = [
    #        PageDocInline,
    #        PagePicInline,
    #          ]


class MetaDataAdminForm(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(
        attrs={
            'size': 100
        }
    ))
    description = forms.CharField(required=False, widget=forms.Textarea(
        attrs={
            'cols': 60,
            'rows': 3
        }
    ))
    keywords = forms.CharField(required=False, widget=forms.Textarea(
        attrs={
            'cols': 60,
            'rows': 3
        }
    ))

    class Meta:
        model = MetaData


class MetaDataAdmin(admin.ModelAdmin):
    list_display = ('url', 'title',)
    list_display_links = ('url', 'title',)
    search_fields = ('url', 'title',)
    form = MetaDataAdminForm

admin.site.register(MetaData, MetaDataAdmin)
admin.site.register(Page, PageAdmin)