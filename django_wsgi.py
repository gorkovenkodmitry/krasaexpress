import os, sys, site

sys.path.insert(0, os.path.dirname(__file__))
site.addsitedir('/usr/local/lib/python2.6/dist-packages')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

from django.core.handlers.wsgi import WSGIHandler
application = WSGIHandler()

import monitor
monitor.start(interval=1.0)