$.fn.slider = function(args) {
	var slides = $(this).find('.slides');
	var width = slides.find('.item').width() + 60;
	var count_slides = slides.find('.item').length;
	var click = true;
	slides.find('.item').each(function(index) {
		$(this).css({
			'left': width*index + 'px',
			'z-index': index + 2
		});
	});
	slides.css({
		'position': 'relative',
		'overflow': 'hidden'
	});
	if (count_slides <= args.count_items) {
		return true;
	}

	var current_position = 0;
	function swipe_to_position(direction) {
		if (direction=='left') {
			var new_position = current_position - 1;
			var data_pos = 0;
			for (var i=0; i<=args.count_items; i++) {
				slides.find('.item:eq('+((current_position+i)%count_slides)+')').attr('data-pos', i);
			}
			slides.find('.item:eq('+((new_position)%count_slides)+')').attr('data-pos', -1);
			slides.find('.item').each(function(index) {
				$(this).css({
					'left': parseInt($(this).attr('data-pos'))*width+'px',
				});
			});
			for (var i=0; i<count_slides; i++) {
				slides.find('.item:eq('+((new_position+i)%count_slides)+')').attr('data-pos', i);
			}
			slides.find('.item').each(function(index) {
				var data_pos = parseInt($(this).attr('data-pos'));
				if (data_pos>-1 && data_pos <= args.count_items) {
					$(this).animate({
						'left': data_pos*width+'px',
					}, args.time_animation)
				}
			});
			current_position=new_position;
		}
		if (direction=='right') {
			var new_position = current_position + 1;
			var data_pos = 0;
			for (var i=0; i<=args.count_items; i++) {
				slides.find('.item:eq('+((current_position+i)%count_slides)+')').attr('data-pos', i);
			}
			slides.find('.item').each(function(index) {
				$(this).css({
					'left': parseInt($(this).attr('data-pos'))*width+'px',
				});
			});
			for (var i=0; i<count_slides; i++) {
				slides.find('.item:eq('+((new_position+i)%count_slides)+')').attr('data-pos', i);
			}
			slides.find('.item:eq('+((new_position-1)%count_slides)+')').attr('data-pos', -1);
			slides.find('.item').each(function(index) {
				var data_pos = parseInt($(this).attr('data-pos'));
				if (data_pos>=-1 && data_pos < args.count_items) {
					$(this).animate({
						'left': data_pos*width+'px',
					}, args.time_animation)
				}
			});
			current_position=new_position;
		}
	}

	var curr_interval = setInterval(function() {
		swipe_to_position('left');
	}, args.time_interval);

	slides.swipe({
		swipeLeft: function(event, direction, distance, duration, fingerCount) {
			if (!click) {
				return false;
			}
			click = false;
			setTimeout(function() { click=true; }, args.time_animation);
			clearInterval(curr_interval);
			swipe_to_position('right');
			curr_interval = setInterval(function() {
				swipe_to_position('left');
			}, args.time_interval);
			return false;
		},
		swipeRight: function(event, direction, distance, duration, fingerCount) {
			if (!click) {
				return false;
			}
			click = false;
			setTimeout(function() { click=true; }, args.time_animation);
			clearInterval(curr_interval);
			swipe_to_position('left');
			curr_interval = setInterval(function() {
				swipe_to_position('left');
			}, args.time_interval);
			return false;
		}
	}); 
}

$(document).ready(function(e) {
	$('input[name="telephone"]').mask('+7  (999) 999 99-99');
	$('input[name="date_order"]').mask('99.99.9999');
	$('a.zoom').fancybox({
		helpers: {
			title: {
				type: 'float'
			},
			thumbs: {
				width: 50,
				height: 50
			}
		}
	});

	$('#reviews').slider({
		'count_items': 3,
		'time_animation': 500,
		'time_interval': 2000
	});

	$('.form').on('click', 'a.submit', function(e) {
		e.preventDefault();
		var href=$(this).attr('href');
		var form = $(this).parent();
		$.ajax({
			url: href,
			type: 'POST',
			data: form.serialize(),
			success: function(data) {
				if (data=='error') {
					alert('Ошибка при заполнении полей');
				}
				if (data=='success') {
					alert('Спасибо, ваша заявка принята');
					var url_value = form.find('input[name="url"]').val();
					form.find('input').val('');
					form.find('input[name="url"]').val(url_value)
				}
			}
		});
		return false;
	});
});