# -*- coding: utf-8 -*-

DATABASE_NAME = u'krasaexpress'
PROJECT_NAME = u'krasaexpress'
SITE_NAME = u'krasaexpress'
DEFAULT_FROM_EMAIL = u'noreply@krasaexpress.ru'
ORDER_EMAIL = u''

from config.base import *

try:
    from config.development import *
except ImportError:
    from config.production import *

TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = DEBUG

INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.staticfiles',

    'mptt',
    'south',
    'pytils',
    'pymorphy',
    'pagination',
    'widget_tweaks',
    'salmonella',
    'sorl.thumbnail',

    'apps.pages',
    'apps.siteblocks',
    'apps.information',
)

LOCALE_PATHS = (
    ROOT_PATH+'/locale/',

)

CAPTCHA_CHALLENGE_FUNC = 'captcha.helpers.math_challenge'

MPTT_ADMIN_LEVEL_INDENT = 20

MIDDLEWARE_CLASSES += (
    'apps.pages.middleware.PageFallbackMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS += (
    'apps.pages.context_processors.meta',
    'apps.siteblocks.context_processors.settings',
    'apps.utils.context_processors.custom_proc',

)

FILEBROWSE_PATH = os.path.join(MEDIA_ROOT, 'uploads/images/')
FILEBROWSE_URL = MEDIA_URL + 'uploads/images/'

HAYSTACK_SEARCH_ENGINE = 'simple'
