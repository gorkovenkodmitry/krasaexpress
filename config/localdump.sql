CREATE DATABASE  IF NOT EXISTS `krasaexpress` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `krasaexpress`;
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: krasaexpress
-- ------------------------------------------------------
-- Server version	5.5.38-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `information_serviceorder`
--

DROP TABLE IF EXISTS `information_serviceorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_serviceorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_date` datetime NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(75) NOT NULL,
  `status` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_serviceorder`
--

LOCK TABLES `information_serviceorder` WRITE;
/*!40000 ALTER TABLE `information_serviceorder` DISABLE KEYS */;
INSERT INTO `information_serviceorder` VALUES (1,'2014-09-27 13:57:29','+7  (222) 222 22-22','312321','3@2.ru','new',''),(2,'2014-09-27 13:57:53','+7  (333) 333 33-33','321321','4@32.rr','new','');
/*!40000 ALTER TABLE `information_serviceorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add site',6,'add_site'),(17,'Can change site',6,'change_site'),(18,'Can delete site',6,'delete_site'),(19,'Can add log entry',7,'add_logentry'),(20,'Can change log entry',7,'change_logentry'),(21,'Can delete log entry',7,'delete_logentry'),(22,'Can add migration history',8,'add_migrationhistory'),(23,'Can change migration history',8,'change_migrationhistory'),(24,'Can delete migration history',8,'delete_migrationhistory'),(25,'Can add kv store',9,'add_kvstore'),(26,'Can change kv store',9,'change_kvstore'),(27,'Can delete kv store',9,'delete_kvstore'),(28,'Can add page_item',10,'add_page'),(29,'Can change page_item',10,'change_page'),(30,'Can delete page_item',10,'delete_page'),(31,'Can add файл',11,'add_pagedoc'),(32,'Can change файл',11,'change_pagedoc'),(33,'Can delete файл',11,'delete_pagedoc'),(34,'Can add картинка',12,'add_pagepic'),(35,'Can change картинка',12,'change_pagepic'),(36,'Can delete картинка',12,'delete_pagepic'),(37,'Can add meta',13,'add_metadata'),(38,'Can change meta',13,'change_metadata'),(39,'Can delete meta',13,'delete_metadata'),(40,'Can add bookmark',14,'add_bookmark'),(41,'Can change bookmark',14,'change_bookmark'),(42,'Can delete bookmark',14,'delete_bookmark'),(43,'Can add dashboard preferences',15,'add_dashboardpreferences'),(44,'Can change dashboard preferences',15,'change_dashboardpreferences'),(45,'Can delete dashboard preferences',15,'delete_dashboardpreferences'),(46,'Can add site_setting',16,'add_settings'),(47,'Can change site_setting',16,'change_settings'),(48,'Can delete site_setting',16,'delete_settings'),(49,'Can add service',17,'add_service'),(50,'Can change service',17,'change_service'),(51,'Can delete service',17,'delete_service'),(52,'Can add project',18,'add_project'),(53,'Can change project',18,'change_project'),(54,'Can delete project',18,'delete_project'),(55,'Can add action_item',19,'add_action'),(56,'Can change action_item',19,'change_action'),(57,'Can delete action_item',19,'delete_action'),(58,'Can add review',20,'add_review'),(59,'Can change review',20,'change_review'),(60,'Can delete review',20,'delete_review'),(61,'Can add social_link',21,'add_sociallink'),(62,'Can change social_link',21,'change_sociallink'),(63,'Can delete social_link',21,'delete_sociallink'),(64,'Can add test_order',22,'add_siteorder'),(65,'Can change test_order',22,'change_siteorder'),(66,'Can delete test_order',22,'delete_siteorder'),(67,'Can add price_order',23,'add_priceorder'),(68,'Can change price_order',23,'change_priceorder'),(69,'Can delete price_order',23,'delete_priceorder'),(70,'Can add service_order',24,'add_serviceorder'),(71,'Can change service_order',24,'change_serviceorder'),(72,'Can delete service_order',24,'delete_serviceorder'),(73,'Can add action_order',25,'add_actionorder'),(74,'Can change action_order',25,'change_actionorder'),(75,'Can delete action_order',25,'delete_actionorder'),(76,'Can add review_order',26,'add_revieworder'),(77,'Can change review_order',26,'change_revieworder'),(78,'Can delete review_order',26,'delete_revieworder'),(79,'Can add question_order',27,'add_questionorder'),(80,'Can change question_order',27,'change_questionorder'),(81,'Can delete question_order',27,'delete_questionorder');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagedoc`
--

DROP TABLE IF EXISTS `pages_pagedoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagedoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagedoc_3fb9c94f` (`page_id`),
  CONSTRAINT `page_id_refs_id_cb73be08` FOREIGN KEY (`page_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagedoc`
--

LOCK TABLES `pages_pagedoc` WRITE;
/*!40000 ALTER TABLE `pages_pagedoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagedoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'site','sites','site'),(7,'log entry','admin','logentry'),(8,'migration history','south','migrationhistory'),(9,'kv store','thumbnail','kvstore'),(10,'page_item','pages','page'),(11,'файл','pages','pagedoc'),(12,'картинка','pages','pagepic'),(13,'meta','pages','metadata'),(14,'bookmark','menu','bookmark'),(15,'dashboard preferences','dashboard','dashboardpreferences'),(16,'site_setting','siteblocks','settings'),(17,'service','siteblocks','service'),(18,'project','siteblocks','project'),(19,'action_item','siteblocks','action'),(20,'review','siteblocks','review'),(21,'social_link','siteblocks','sociallink'),(22,'test_order','information','siteorder'),(23,'price_order','information','priceorder'),(24,'service_order','information','serviceorder'),(25,'action_order','information','actionorder'),(26,'review_order','information','revieworder'),(27,'question_order','information','questionorder');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_service`
--

DROP TABLE IF EXISTS `siteblocks_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `image` varchar(100) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_service`
--

LOCK TABLES `siteblocks_service` WRITE;
/*!40000 ALTER TABLE `siteblocks_service` DISABLE KEYS */;
INSERT INTO `siteblocks_service` VALUES (1,'ПРИЧЁСКА','images/services/1.jpg',10,1),(2,'СТРИЖКА','images/services/4_1.jpg',10,1),(3,'МАНИКЮР','images/services/5.jpg',10,1),(4,'ПЕДИКЮР','images/services/6.jpg',10,1),(5,'МАКИЯЖ','images/services/7.jpg',10,1),(6,'АКВАГРИММ, БЛЕСК-ТАТУ','images/services/8.jpg',10,1);
/*!40000 ALTER TABLE `siteblocks_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-09-27 13:07:48',1,17,'1','ПРИЧЁСКА',1,''),(2,'2014-09-27 13:08:05',1,17,'2','СТРИЖКА',1,''),(3,'2014-09-27 13:10:45',1,17,'1','ПРИЧЁСКА',2,'Ни одно поле не изменено.'),(4,'2014-09-27 13:10:56',1,17,'1','ПРИЧЁСКА',2,'Изменен image.'),(5,'2014-09-27 13:11:22',1,17,'3','МАНИКЮР',1,''),(6,'2014-09-27 13:11:59',1,17,'4','ПЕДИКЮР',1,''),(7,'2014-09-27 13:12:17',1,17,'5','МАКИЯЖ',1,''),(8,'2014-09-27 13:12:33',1,17,'6','АКВАГРИММ, БЛЕСК-ТАТУ',1,''),(9,'2014-09-27 13:14:02',1,18,'1','',1,''),(10,'2014-09-27 13:14:08',1,18,'2','',1,''),(11,'2014-09-27 13:14:14',1,18,'3','',1,''),(12,'2014-09-27 13:14:19',1,18,'4','',1,''),(13,'2014-09-27 13:14:25',1,18,'5','',1,''),(14,'2014-09-27 13:14:32',1,18,'6','',1,''),(15,'2014-09-27 13:14:36',1,18,'7','',1,''),(16,'2014-09-27 13:14:41',1,18,'8','',1,''),(17,'2014-09-27 13:14:46',1,18,'9','',1,''),(18,'2014-09-27 13:19:54',1,19,'1','Свадебный комплекс услуг! Скидки до 10%!',1,''),(19,'2014-09-27 13:20:06',1,19,'2','Счастливый понедельник! Скидка в понедельник до 15%!',1,''),(20,'2014-09-27 13:20:18',1,19,'3','А у нас корпоратив! Каждая пятая прическа — БЕСПЛАТНО!',1,''),(21,'2014-09-27 13:26:44',1,20,'1','1',1,''),(22,'2014-09-27 13:27:18',1,20,'2','2',1,''),(23,'2014-09-27 13:28:01',1,20,'3','3',1,''),(24,'2014-09-27 13:29:46',1,21,'1','SocialLink object',1,''),(25,'2014-09-27 13:31:08',1,21,'2','SocialLink object',1,''),(26,'2014-09-27 13:31:50',1,21,'3','SocialLink object',1,''),(27,'2014-09-27 13:43:51',1,16,'1','site_email',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_prefer_dashboard_id_374bce90a8a4eefc_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_23127ba7` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{}','siteblocks-dashboard'),(3,1,'{}','information-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_revieworder`
--

DROP TABLE IF EXISTS `information_revieworder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_revieworder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_date` datetime NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(75) NOT NULL,
  `status` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_revieworder`
--

LOCK TABLES `information_revieworder` WRITE;
/*!40000 ALTER TABLE `information_revieworder` DISABLE KEYS */;
INSERT INTO `information_revieworder` VALUES (1,'2014-09-27 14:05:37','+7  (888) 888 88-88','ddddd','1@2.ru','new','');
/*!40000 ALTER TABLE `information_revieworder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_metadata`
--

DROP TABLE IF EXISTS `pages_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_metadata`
--

LOCK TABLES `pages_metadata` WRITE;
/*!40000 ALTER TABLE `pages_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_project`
--

DROP TABLE IF EXISTS `siteblocks_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `image` varchar(100) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_project`
--

LOCK TABLES `siteblocks_project` WRITE;
/*!40000 ALTER TABLE `siteblocks_project` DISABLE KEYS */;
INSERT INTO `siteblocks_project` VALUES (1,'','images/projects/9.jpg',10,1),(2,'','images/projects/10.jpg',10,1),(3,'','images/projects/11.jpg',10,1),(4,'','images/projects/12.jpg',10,1),(5,'','images/projects/13.jpg',10,1),(6,'','images/projects/14.jpg',10,1),(7,'','images/projects/15.jpg',10,1),(8,'','images/projects/16.jpg',10,1),(9,'','images/projects/17.jpg',10,1);
/*!40000 ALTER TABLE `siteblocks_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
INSERT INTO `thumbnail_kvstore` VALUES ('sorl-thumbnail||image||0a2f70d99fe98b49043d08f522040971','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a9/6d/a96d95215b7a3681e38892647c5e9e72.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||0c191a85105dcd863c1857dbbbefa132','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/8a/39/8a39ce899fbdb8ba7dfcccf20bbdfc40.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||0e5afdc5bd33c67caac607716761467f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/bb/48/bb487e4b3c8b3072961fdf18202e401a.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||0f80de94fc2ad7c8f0c981b469d56afc','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/92/f4/92f49ff699bf45cac4b7c4381196c01c.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||108db14f16a5f69fcf8999c96fd811aa','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/b0/1e/b01ef1da1e8d3128afe48edd17e46929.png\", \"size\": [30, 30]}'),('sorl-thumbnail||image||148a4803d802daefa2927ac5daeab9d6','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/b8/9d/b89daaa5917facf0d8b8a2dd70c67e28.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||18a97f66e85fa5751688661e5c4011e5','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/12.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||1cf444ae021680d288ca7657904d6eb0','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/32/df/32dfb71e98cf3a27a47b02883ab94832.png\", \"size\": [30, 30]}'),('sorl-thumbnail||image||1ebf349f18c7b4de4a63632c8035f180','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/d8/bd/d8bdd7c9f83cad5f166a0f9519cb73bf.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||1f8f12dbad80a769df518af72e50c4be','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/2a/e1/2ae10ef669b5799eb2a2461ea589fd31.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||2003ba0911b5dae742cd5f13ebeed05d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/11.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||213846f3713437a3ce373f6b2a787626','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/actions/18.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||224643d5440c89ef86ebe118736644ca','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/5a/b4/5ab4a4c318c4b388777ecc5f84f03cfb.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||282a84ec9f4bb9c9f74a8c057f9508f8','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/9e/66/9e66e864911a51b7e238d83815cdd1e3.png\", \"size\": [30, 30]}'),('sorl-thumbnail||image||29e92c7b6cd53fc1c0ec67e2960fe57a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/4_1.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||2dd65684900fe840868ab70e335776be','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/6.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||313fdb2d8bc6a50e2066a1770bea05c1','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/5d/c9/5dc9eae2c81c13bfac382f114ca24b88.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||3621f72b39791abd31a225601f0a5779','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/14.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||395ac8613ce950301f9b44bc411c80c7','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/9b/5b/9b5b1d584d02495488fbefe83c0d0888.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||39dfeacef10734306bde39b5048fbef0','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/32/28/3228c8a2cba09fc97792167d8fa64250.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||3cc047a97c6390efa0b64a1506c71b11','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/f2/f2/f2f23228509ad800e32eb865fd587ce7.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||3deea0c4c1c137659da74bc07baf3b57','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/00/74/0074f1cb4008b3a26f4014713df02b04.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||43383f7997962700e389d81bce40fcb3','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/95/d0/95d0df24b5d6b79da209e5ca41ef47d1.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||452f03d26de88c7409f48e89ba2eff34','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/e3/6d/e36de1a0cf5fc2e98c866031eb44728c.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||496695f6088b5d60dbb35cfe12a343fa','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/17.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||49d6ff731f63169141dbc727fe5a1b8b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a6/4d/a64d7adf4002294f7508d6b697180d39.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||4eef535f0c5fe159f5982d789d3ef9ac','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/39/09/390924a1c69023564c719016b0c04690.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||5501eeece107daae442c48a11b8b7be8','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/b2/b4/b2b41393dd292ad48ed9b6c5c06f0b9a.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||58497ed36daee7142652e8c806a6b608','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/16.jpg\", \"size\": [312, 211]}'),('sorl-thumbnail||image||5fc59e937f18fd7cac5f2ec276af23f5','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/2c/96/2c96f7417cf10ef5063b37618e8d33f4.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||63d8ffeaa4922f2adabc180978694a62','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/4.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||65afbf1697c4d4cf2d25e0e7cefb7c4b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/89/2f/892fab371c1d4cff096fd1a621ba9783.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||67f37d5b921c89daacc73379032abd0f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/actions/2.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||6bc58856f04360ef61b189e484038458','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/62/97/6297301f17c0e3e9f58663534bfa6816.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||6c015cc911b00e9fd91ce69075c3b45b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/b1/75/b175437a4501e42ac7c9bb919916dee4.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||7489fc42461bf3858311507e26dddcc4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/actions/19.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||7bec833b1fa5cdf399deef68e51f9a39','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/05/43/0543547f635b3913f8fb152234a8b1be.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||7c829d3bfdabdc0f2f91c35ab35a1404','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/94/87/9487df57d9bc8c49be69edfdfa4ecff6.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||7d2999e0bf11730f26c276b3894a6f9d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/10.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||7f580cc78fb78604efd254989df3509a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/37/2f/372f87ccb1ae65d1f0a7a0aa266dec97.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||8231aa0f36a5207bf00ac0a1d003466c','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/social_links/2.png\", \"size\": [65, 65]}'),('sorl-thumbnail||image||84856aca068223a18effc5ecdbfa4512','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/77/b8/77b81a30894fffe48735873d14f1e942.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||86c843462a968d404b7d5c11a474dbee','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/30/12/3012de54989acb680b818936a34ca548.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||8eb5368357ae7c158fe32bcc8627b999','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/15.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||8f5ff9f3dfef0d976d6e1cd3f5702693','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/c1/9e/c19e39ee8d51ba2f35e7c68e0c0f7a5b.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||9040a439f1295559a0c7f6efb92959c7','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/3a/36/3a36d53cd3e54559dcad79d31ffbdcf4.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||97528bbd3bb49c36fb4f006ca8bfd750','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/af/c3/afc362bc25c3578ccd8542e6c0618c0a.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||981767ac32351eff9e951d2fc5724054','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ff/97/ff97fb35d7561cd66f3ac6bd15bb29f7.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||9d2ce1c41d30c0fe68dc1d14e729c5a0','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/82/1a/821a786dd943fde43a787ab3ecb0a71f.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||ace1fa1d2e76bbaef39b9b85449e0ea5','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a1/7e/a17e843af36170ced104c3cdbaa74343.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||b03ca5f78640e2a4b15fe28d98ae6135','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/10/b0/10b0772e8f06fb2f866b5b0a1ba556cc.png\", \"size\": [65, 65]}'),('sorl-thumbnail||image||b0c49f189e1c1ffef406261f88809d47','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/actions/3.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||b5833c184335e3a9d47625e3a1b4b7b7','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/68/62/68622e69cf01e1770522cf548ceadd50.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||b660bf97a350f8d1cf90dbba789db149','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/1.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||b692baa2875063610b2912443734c070','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/c2/7e/c27e5540f822443ef4fac9dc779c5245.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||b899444d8eb60b5683fce0ad9c9b04ed','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/social_links/3.png\", \"size\": [65, 65]}'),('sorl-thumbnail||image||ba1d949dbce5d33f18b78e22444dcc5b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/7.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||bf77ef6d46286eb2991604d7904bff3b','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/6e/bd/6ebd475168bf76f8c0ce57de6f520cc9.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||c1df923b6ae35a1d08d2f549e55ba264','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/13.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||c2384c7350b618cc65ba7fe4542ab37f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/7d/5c/7d5c53ae3c824762bed7715987705ea4.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||c78abada934848fbfc30aeb0e962937e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/d8/b2/d8b2c43646d54d886545d9875ac4f6cd.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||c95dfebdd843ce3ac476bc5c29c6d034','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/social_links/1.png\", \"size\": [65, 65]}'),('sorl-thumbnail||image||ca64fe6b94b5bfda0401c674194a2f7e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/fa/43/fa43bd57dff49f88ec8226c8faa2f101.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||cf70e51556bd88fdff014201aab35110','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/c0/e0/c0e02e02986455c72e3210c9726b6140.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||d1fb9e1a7b4ff3e312900d60c7e4e020','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/actions/21.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||d3ce89b7a11b02e5407e60c02ca20727','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/88/b9/88b90def99359697cdfecc24eb904edc.png\", \"size\": [65, 65]}'),('sorl-thumbnail||image||d5acd435dc2e0e64821d91b8c31745fa','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/66/7a/667a86fa9dab363a193a0e8c3bb928f4.png\", \"size\": [65, 65]}'),('sorl-thumbnail||image||d7bb25d6b03bf7da702486bc14a95392','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/actions/20.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||dc089f029372c3ea087f6963c94f6395','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/f6/be/f6be05538b496bcddb331c51803a30cd.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||dc62d4bbf35fec5643a508b89ec8cc77','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/65/0c/650cdc60a19f12e988ccf36073ce5031.jpg\", \"size\": [214, 214]}'),('sorl-thumbnail||image||deb69c21d3fd282682339c6ea328755f','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/da/60/da607951b973e93633337487583288f1.png\", \"size\": [96, 96]}'),('sorl-thumbnail||image||e9796b2a92cb6daa547cd837c0cfa5c4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/5.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||eaec97606550cce854ec078520c27008','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/services/8.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||ed0b035a37003fb150621b9d5cc1afa4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"images/projects/9.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||image||f286275920a34826d10d3e16b938ebe4','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/58/f3/58f3b14ae1955a7337a90848e9bb4727.jpg\", \"size\": [310, 211]}'),('sorl-thumbnail||thumbnails||18a97f66e85fa5751688661e5c4011e5','[\"0f80de94fc2ad7c8f0c981b469d56afc\", \"f286275920a34826d10d3e16b938ebe4\"]'),('sorl-thumbnail||thumbnails||2003ba0911b5dae742cd5f13ebeed05d','[\"6c015cc911b00e9fd91ce69075c3b45b\", \"86c843462a968d404b7d5c11a474dbee\"]'),('sorl-thumbnail||thumbnails||213846f3713437a3ce373f6b2a787626','[\"dc62d4bbf35fec5643a508b89ec8cc77\", \"3deea0c4c1c137659da74bc07baf3b57\"]'),('sorl-thumbnail||thumbnails||29e92c7b6cd53fc1c0ec67e2960fe57a','[\"395ac8613ce950301f9b44bc411c80c7\", \"0e5afdc5bd33c67caac607716761467f\"]'),('sorl-thumbnail||thumbnails||2dd65684900fe840868ab70e335776be','[\"43383f7997962700e389d81bce40fcb3\", \"84856aca068223a18effc5ecdbfa4512\"]'),('sorl-thumbnail||thumbnails||3621f72b39791abd31a225601f0a5779','[\"9d2ce1c41d30c0fe68dc1d14e729c5a0\", \"452f03d26de88c7409f48e89ba2eff34\"]'),('sorl-thumbnail||thumbnails||496695f6088b5d60dbb35cfe12a343fa','[\"4eef535f0c5fe159f5982d789d3ef9ac\", \"3cc047a97c6390efa0b64a1506c71b11\"]'),('sorl-thumbnail||thumbnails||58497ed36daee7142652e8c806a6b608','[\"7bec833b1fa5cdf399deef68e51f9a39\", \"313fdb2d8bc6a50e2066a1770bea05c1\"]'),('sorl-thumbnail||thumbnails||63d8ffeaa4922f2adabc180978694a62','[\"b5833c184335e3a9d47625e3a1b4b7b7\", \"65afbf1697c4d4cf2d25e0e7cefb7c4b\"]'),('sorl-thumbnail||thumbnails||67f37d5b921c89daacc73379032abd0f','[\"224643d5440c89ef86ebe118736644ca\", \"c78abada934848fbfc30aeb0e962937e\"]'),('sorl-thumbnail||thumbnails||7489fc42461bf3858311507e26dddcc4','[\"1ebf349f18c7b4de4a63632c8035f180\", \"cf70e51556bd88fdff014201aab35110\"]'),('sorl-thumbnail||thumbnails||7d2999e0bf11730f26c276b3894a6f9d','[\"39dfeacef10734306bde39b5048fbef0\", \"8f5ff9f3dfef0d976d6e1cd3f5702693\"]'),('sorl-thumbnail||thumbnails||8231aa0f36a5207bf00ac0a1d003466c','[\"282a84ec9f4bb9c9f74a8c057f9508f8\", \"d3ce89b7a11b02e5407e60c02ca20727\"]'),('sorl-thumbnail||thumbnails||8eb5368357ae7c158fe32bcc8627b999','[\"0c191a85105dcd863c1857dbbbefa132\", \"9040a439f1295559a0c7f6efb92959c7\"]'),('sorl-thumbnail||thumbnails||b0c49f189e1c1ffef406261f88809d47','[\"5fc59e937f18fd7cac5f2ec276af23f5\", \"5501eeece107daae442c48a11b8b7be8\"]'),('sorl-thumbnail||thumbnails||b660bf97a350f8d1cf90dbba789db149','[\"981767ac32351eff9e951d2fc5724054\", \"0a2f70d99fe98b49043d08f522040971\"]'),('sorl-thumbnail||thumbnails||b899444d8eb60b5683fce0ad9c9b04ed','[\"b03ca5f78640e2a4b15fe28d98ae6135\", \"108db14f16a5f69fcf8999c96fd811aa\"]'),('sorl-thumbnail||thumbnails||ba1d949dbce5d33f18b78e22444dcc5b','[\"49d6ff731f63169141dbc727fe5a1b8b\", \"bf77ef6d46286eb2991604d7904bff3b\"]'),('sorl-thumbnail||thumbnails||c1df923b6ae35a1d08d2f549e55ba264','[\"148a4803d802daefa2927ac5daeab9d6\", \"1f8f12dbad80a769df518af72e50c4be\"]'),('sorl-thumbnail||thumbnails||c95dfebdd843ce3ac476bc5c29c6d034','[\"1cf444ae021680d288ca7657904d6eb0\", \"d5acd435dc2e0e64821d91b8c31745fa\"]'),('sorl-thumbnail||thumbnails||d1fb9e1a7b4ff3e312900d60c7e4e020','[\"deb69c21d3fd282682339c6ea328755f\", \"97528bbd3bb49c36fb4f006ca8bfd750\"]'),('sorl-thumbnail||thumbnails||d7bb25d6b03bf7da702486bc14a95392','[\"7f580cc78fb78604efd254989df3509a\", \"ace1fa1d2e76bbaef39b9b85449e0ea5\"]'),('sorl-thumbnail||thumbnails||e9796b2a92cb6daa547cd837c0cfa5c4','[\"b692baa2875063610b2912443734c070\", \"c2384c7350b618cc65ba7fe4542ab37f\"]'),('sorl-thumbnail||thumbnails||eaec97606550cce854ec078520c27008','[\"7c829d3bfdabdc0f2f91c35ab35a1404\", \"dc089f029372c3ea087f6963c94f6395\"]'),('sorl-thumbnail||thumbnails||ed0b035a37003fb150621b9d5cc1afa4','[\"ca64fe6b94b5bfda0401c674194a2f7e\", \"6bc58856f04360ef61b189e484038458\"]');
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_priceorder`
--

DROP TABLE IF EXISTS `information_priceorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_priceorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_date` datetime NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(75) NOT NULL,
  `status` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_priceorder`
--

LOCK TABLES `information_priceorder` WRITE;
/*!40000 ALTER TABLE `information_priceorder` DISABLE KEYS */;
INSERT INTO `information_priceorder` VALUES (1,'2014-09-27 13:52:19','+7  (321) 222 22-22','123','1@1.ru','new','');
/*!40000 ALTER TABLE `information_priceorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_review`
--

DROP TABLE IF EXISTS `siteblocks_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_review`
--

LOCK TABLES `siteblocks_review` WRITE;
/*!40000 ALTER TABLE `siteblocks_review` DISABLE KEYS */;
INSERT INTO `siteblocks_review` VALUES (1,'Недавно у брата была свадьба и мне сделали замечательный макияж, который, кстати, продержался весь день! Сделала мастер Наталья, все были в восторге от её работы. Вот уж действительно, золотые руки у Наташи. Так что теперь я знаю куда обращаться за макияжем...','images/actions/2.jpg',10,1),(2,'Приятно была удивлена, что у нас в городе есть такая услугу — «Мобильный салон». Очень удобно. Огромное спасибо мастеру Анастасие за создание моей cвадебной прически! Переживала при заказе, но приехали вовремя, а прическа получилась очень красивой. Буду обращаться еще и советовать подружкам...','images/actions/20.jpg',10,1),(3,'Хочу сказать «СПАСИБО!» этому салону! С моей загруженностью на работе просто некогда разъезжать по салонам. Собиралась на ужин с коллегами и заказала мастеров. Укладку и макияж сделали превосходно! Советую, удобно.','images/actions/21.jpg',10,1);
/*!40000 ALTER TABLE `siteblocks_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_1dc7bd98` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('dp696cfllne03emkiqs2t2m61w3px3dx','M2FjNTUxODdiMzlkYmIyNThjY2UxODgwYzk2YzhjOTgxMjY0ZjA4Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-10-11 13:07:21'),('l8zyjpw64hgogiml6k8m3pvv821q0dx4','M2FjNTUxODdiMzlkYmIyNThjY2UxODgwYzk2YzhjOTgxMjY0ZjA4Zjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-10-11 13:10:38');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2014-09-27 13:05:54'),(2,'dashboard','0001_initial','2014-09-27 13:05:55'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2014-09-27 13:05:56'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2014-09-27 13:05:56'),(5,'siteblocks','0001_initial','2014-09-27 13:06:37'),(6,'information','0001_initial','2014-09-27 13:06:38'),(7,'information','0002_auto__del_field_siteorder_email__add_field_siteorder_name','2014-09-27 13:10:10'),(8,'siteblocks','0002_auto__chg_field_action_name','2014-09-27 13:20:59'),(9,'siteblocks','0003_auto__chg_field_review_text','2014-09-27 13:26:30'),(10,'information','0003_auto__add_priceorder','2014-09-27 13:51:44'),(11,'information','0004_auto__add_serviceorder','2014-09-27 13:54:37'),(12,'information','0005_auto__add_actionorder','2014-09-27 14:01:16'),(13,'information','0006_auto__add_revieworder','2014-09-27 14:05:27'),(14,'information','0007_auto__add_questionorder','2014-09-27 14:06:33');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_sociallink`
--

DROP TABLE IF EXISTS `siteblocks_sociallink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_sociallink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `url` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_sociallink`
--

LOCK TABLES `siteblocks_sociallink` WRITE;
/*!40000 ALTER TABLE `siteblocks_sociallink` DISABLE KEYS */;
INSERT INTO `siteblocks_sociallink` VALUES (1,'Mail','http://mail.ru/','images/social_links/1.png',10,1),(2,'Одноклассники','http://ok.ru/','images/social_links/2.png',10,1),(3,'Вконтакте','http://vk.com/','images/social_links/3.png',10,1);
/*!40000 ALTER TABLE `siteblocks_sociallink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_questionorder`
--

DROP TABLE IF EXISTS `information_questionorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_questionorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_date` datetime NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(75) NOT NULL,
  `status` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_questionorder`
--

LOCK TABLES `information_questionorder` WRITE;
/*!40000 ALTER TABLE `information_questionorder` DISABLE KEYS */;
INSERT INTO `information_questionorder` VALUES (1,'2014-09-27 14:09:00','+7  (666) 666 66-66','rrrrrrrrrrrrrr','1@1.ru','new','');
/*!40000 ALTER TABLE `information_questionorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_actionorder`
--

DROP TABLE IF EXISTS `information_actionorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_actionorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_date` datetime NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(75) NOT NULL,
  `status` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_actionorder`
--

LOCK TABLES `information_actionorder` WRITE;
/*!40000 ALTER TABLE `information_actionorder` DISABLE KEYS */;
INSERT INTO `information_actionorder` VALUES (1,'2014-09-27 14:01:19','+7  (434) 444 44-44','dasdsadsa','4dsa@das.riu','new','');
/*!40000 ALTER TABLE `information_actionorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_page`
--

DROP TABLE IF EXISTS `pages_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  `title` varchar(120) NOT NULL,
  `url` varchar(200) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `content` longtext NOT NULL,
  `type` varchar(20) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `template` varchar(100) NOT NULL,
  `page_title` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(250) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `pages_page_410d0aac` (`parent_id`),
  KEY `pages_page_329f6fb3` (`lft`),
  KEY `pages_page_e763210f` (`rght`),
  KEY `pages_page_ba470c4a` (`tree_id`),
  KEY `pages_page_20e079f4` (`level`),
  CONSTRAINT `parent_id_refs_id_68963b8e` FOREIGN KEY (`parent_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_page`
--

LOCK TABLES `pages_page` WRITE;
/*!40000 ALTER TABLE `pages_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$Zu2WmCQ0U5zP$X5x0IxuC3Himg91TpWNhyKnQ1EFusMdz8TCJZmLUGAg=','2014-09-27 13:10:38',1,'admin','','','1@1.ru',1,1,'2014-09-27 13:05:44');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_settings`
--

DROP TABLE IF EXISTS `siteblocks_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `value` longtext NOT NULL,
  `file` varchar(300) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_settings`
--

LOCK TABLES `siteblocks_settings` WRITE;
/*!40000 ALTER TABLE `siteblocks_settings` DISABLE KEYS */;
INSERT INTO `siteblocks_settings` VALUES (1,'Email сайта','site_email','strange1231@rambler.ru','','input');
/*!40000 ALTER TABLE `siteblocks_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteblocks_action`
--

DROP TABLE IF EXISTS `siteblocks_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteblocks_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `order` int(11) NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteblocks_action`
--

LOCK TABLES `siteblocks_action` WRITE;
/*!40000 ALTER TABLE `siteblocks_action` DISABLE KEYS */;
INSERT INTO `siteblocks_action` VALUES (1,'Свадебный комплекс услуг! Скидки до 10%!','images/actions/3.jpg',10,1),(2,'Счастливый понедельник! Скидка в понедельник до 15%!','images/actions/18.jpg',10,1),(3,'А у нас корпоратив! Каждая пятая прическа — БЕСПЛАТНО!','images/actions/19.jpg',10,1);
/*!40000 ALTER TABLE `siteblocks_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information_siteorder`
--

DROP TABLE IF EXISTS `information_siteorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information_siteorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_date` datetime NOT NULL,
  `date_order` datetime NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `status` varchar(100) NOT NULL,
  `url` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information_siteorder`
--

LOCK TABLES `information_siteorder` WRITE;
/*!40000 ALTER TABLE `information_siteorder` DISABLE KEYS */;
INSERT INTO `information_siteorder` VALUES (1,'2014-09-27 13:35:26','2014-11-11 00:00:00','+7  (111) 111 11-11','new','','123'),(2,'2014-09-27 13:35:50','2014-12-04 00:00:00','+7  (444) 444 44-44','new','','444'),(3,'2014-09-27 13:44:03','2014-12-12 00:00:00','+7  (111) 111 11-11','new','','123'),(4,'2014-09-27 13:44:44','2014-12-12 00:00:00','+7  (111) 111 11-11','new','','3213');
/*!40000 ALTER TABLE `information_siteorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_pagepic`
--

DROP TABLE IF EXISTS `pages_pagepic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_pagepic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `size` int(11) NOT NULL,
  `order` int(10) unsigned NOT NULL,
  `file` varchar(100) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_pagepic_3fb9c94f` (`page_id`),
  CONSTRAINT `page_id_refs_id_83967181` FOREIGN KEY (`page_id`) REFERENCES `pages_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_pagepic`
--

LOCK TABLES `pages_pagepic` WRITE;
/*!40000 ALTER TABLE `pages_pagepic` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_pagepic` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-27 14:14:31
