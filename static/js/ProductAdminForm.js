$(function(){
    $('#id_discount').parent().parent().hide();

    var status = $('#id_status').val();

    if (status){
        if (status=='sale'){
            $('#id_discount').parent().parent().show();
        }
    }

    $('#id_status').change(function(){
        var val = $(this).val();

        if (val=='sale'){
            $('#id_discount').parent().parent().show();
        }
        else{
            $('#id_discount').parent().parent().hide();

        }

    });

});


